(let ((lisp-directory (expand-file-name "lisp" user-emacs-directory)))
  (add-to-list 'load-path lisp-directory)
  (setq custom-file (expand-file-name "init-local.el" lisp-directory)))


(require 'init-package)
(require 'init-local nil t)
(require 'init-config)
(require 'init-theme)
(require 'init-global)
(require 'init-clojure)
(require 'init-markdown)
(require 'init-racket)


(bind-global-keys
 "f" '("files")
 "f f" '("open file" . find-file)
 "f t" '("file tree" . neotree-toggle)
 "f o" '("fuzzy open" . ivy/fuzzy-open))

(bind-global-keys
 "t" '("toggle")
 "t r" '("relative numbers" . linum-relative-mode)
 ;"t P" '("parinfer mode" . parinfer-toggle-mode)
 "t p" '("rainbow parens" . rainbow-delimiters-mode)
 "t s" '("syntax checker" . flycheck-mode))

(bind-global-keys
 "TAB" '("next buffer" . emacs/switch-to-buffer-alternate)
 "SPC" '("command" . counsel-M-x))

(bind-global-keys
 "w" '("window")
 "w s" '("split window horizntal" . split-window-horizontally)
 "w m" '("toggle maximize window" . toggle-maximize-buffer)
 "w n" '("new window" . split-window-horizontally)
 "w v" '("split window horizntal" . split-window-vertically)
 "w j" '("window down" . evil-window-down)
 "w k" '("window up" . evil-window-up)
 "w l" '("window right" . evil-window-right)
 "w h" '("window left" . evil-window-left)
 "w L" '("move window right" . evil-window-move-far-right)
 "w H" '("move window left" . evil-window-move-far-left)
 "w J" '("move window bottom" . evil-window-move-very-bottom)
 "w K" '("move window top" . evil-window-move-very-top)
 "w d" '("delete window" . evil-window-delete))

(bind-global-keys
 "z" '("zoom")
 "z s" '("scale text" .  font/adjust))

(bind-global-keys
 "r" '("replace")
 "r s" '("replace-string" .  replace-string)
 "r r" '("replace-regexp" .  replace-regexp))

(bind-global-keys
 "b" '("buffer")
 "b b" '("switch buffer" .  counsel-switch-buffer)
 "b d" '("delete buffer" . emacs/kill-current-buffer)
 "b s" '("save buffer" . save-buffer))

(bind-global-keys
 "a" '("admin")
 "a s" '("shell")
 "a s t" '("start ansi term" . ansi-term/new-window-bottom))

(bind-global-keys
 "s" '("search")
 "s a" '("ag")
 "s a f" '("search in dir" . ivy/search))

(bind-global-keys
 "j" '("jump")
 "j j" '("jump to char" . evil-avy-goto-word-1)
 "j w" '("jump to char" . evil-avy-goto-char-timer)
 "j l" '("jump to char" . evil-avy-goto-line))

(bind-global-keys
 "k" '("bookmarks")
 "k s" '("bookmark file" . bookmark-set-no-overwrite)
 "k d" '("bookmark delete" . ivy/bookmarks-delete)
 "k k" '("open bookmarks" . counsel-bookmark)
 "k S" '("save bookmarks" . bookmark-save))


(bind-mode-keys
 '(clojure-mode clojurescript-mode clojurec-mode cider-repl-mode)
 "m" '("clojure")
 "m '" '("start repl" . cider-jack-in)
 "m =" '("format buffer" . cider-format-buffer)

 "m l" '("linter")
 "m l n" '("next lint error" . flycheck-next-error)
 "m l p" '("next lint error" . flycheck-previous-error)

 "m h" '("help")
 "m h h" '("show doc" . clojure/doc-at-point)

 "m r" '("refactor")
 "m r n" '("clean ns" . cljr-clean-ns)
 "m r c" '("extract constant" . cljr-extract-constant)
 "m r d" '("extract def" . cljr-extract-def)

 "m g" '("go to")
 "m g g" '("go to function definition" . clojure/find-var-at-point)

 "m s" '("send to repl")
 "m s c" '("clear repl" . cider-repl-clear-buffer)
 "m s b" '("send buffer to repl" . cider-eval-buffer)
 "m s r" '("send region to repl" . clojure/insert-region-in-repl)
 "m s e" '("send last sexpr to repl" . clojure/insert-last-sexp-in-repl)
 "m s f" '("send defun to repl" . clojure/insert-defun-in-repl))


(bind-mode-keys
 `(clojurescript-mode)
 "m '" '("start node repl" . clojurescript/jack-in-node))

(bind-mode-keys
 '(scheme-mode)
 "m" '("scheme")
 "m '" '("start repl" . run-scheme)
 "m s" '("send to repl")
 "m s r" '("send region to repl" . scheme-send-region)
 "m s e" '("send last sexpr to repl" . scheme-send-last-sexp)
 "m s f" '("send defun to repl" . scheme-send-definition))


(bind-mode-keys
 '(racket-mode racket-repl-mode)
 "m" '("racket")
 "m '" '("start repl" . racket-run)
 "m g" '("go to")
 "m g g" '("go to function definition" . racket-xp-visit-definition)
 "m g b" '("go back from definition" . racket-unvisit)
 "m s" '("send to repl")
 "m s r" '("send region to repl" . racket-send-region)
 "m s e" '("send last sexpr to repl" . racket-send-last-sexp)
 "m s f" '("send defun to repl" . racket-send-definition)
 "m s c" '("clear repl" . racket-repl-clear-leaving-last-prompt))


