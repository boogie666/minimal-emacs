(use-package simple-modeline
  :ensure t
  :init
  (simple-modeline-mode 1))

(use-package all-the-icons
  :ensure t
  :init
  (progn
    (load-theme 'atom-one-dark t)))

(use-package atom-one-dark-theme
  :ensure t)

(use-package smart-mode-line-atom-one-dark-theme
  :ensure t)

(use-package smart-mode-line
  :ensure t
  :init
  (progn
    (setq sml/theme 'atom-one-dark)
    (sml/setup)))



(provide 'init-theme)
