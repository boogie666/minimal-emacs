(defalias 'yes-or-no-p 'y-or-n-p)

(setq gc-cons-threshold (* 200 1024 1024))

(setq scroll-conservatively 100)
(setq inhibit-startup-screen t)
(setenv "PATH" (concat (getenv "PATH") ":/usr/local/bin"))
(setq exec-path (append exec-path '("/usr/local/bin")))
(setq scheme-program-name "chez")

(setq ring-bell-function 'ignore)
(set-frame-font "Roboto Mono:15" t t)
(tool-bar-mode 0)
(menu-bar-mode 0)
(toggle-scroll-bar -1)
(setq create-lockfiles nil)

(defvar user-temporary-file-directory
  (concat  "/tmp/" (user-login-name)))

(make-directory user-temporary-file-directory t)
(make-directory (concat user-emacs-directory "backup") t)

(setq backup-by-copying t)

(setq auto-save-directory user-temporary-file-directory)

(setq auto-save-list-file-prefix 
      (concat user-temporary-file-directory ".auto-saves-"))

(setq bkup-backup-directory-info
      `((t ,user-temporary-file-directory full-path)))

(setq backup-by-copying t    ; Don't delink hardlinks
      version-control t      ; Use version numbers on backups
      delete-old-versions t  ; Automatically delete excess backups
      kept-new-versions 3    ; how many of the newest versions to keep
      kept-old-versions 1)   ; and how many of the old

    ;; Save all tempfiles in $TMPDIR/emacs$UID/                                                        
(defconst emacs-tmp-dir (expand-file-name (format "emacs%d" (user-uid)) user-temporary-file-directory))

(setq backup-directory-alist
    `((".*" . ,emacs-tmp-dir)))
(setq auto-save-file-name-transforms
    `((".*" ,emacs-tmp-dir t)))
(setq auto-save-list-file-prefix
    emacs-tmp-dir)


;; fix wierd graphical glich on linux with i3
(when (eq 'gnu/linux system-type)
    (toggle-frame-fullscreen)
    (toggle-frame-fullscreen))


(use-package diminish
  :ensure t)

(use-package eldoc
  :diminish eldoc-mode)


(provide 'init-config)

