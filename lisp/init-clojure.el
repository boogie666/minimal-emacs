(use-package clojure-mode :ensure t)

(use-package flycheck-clj-kondo
  :ensure t
  :init
  (add-hook 'clojurescript-mode-hook 'flycheck/conditional-toggle)
  (add-hook 'clojurec-mode-hook 'flycheck/conditional-toggle)
  (add-hook 'clojure-mode-hook 'flycheck/conditional-toggle))

(use-package rainbow-delimiters
  :ensure t
  :init
  (add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
  (add-hook 'cider-repl-mode-hook 'rainbow-delimiters-mode)
  (add-hook 'clojurescript-mode-hook 'rainbow-delimiters-mode)
  (add-hook 'clojurec-mode-hook 'rainbow-delimiters-mode)
  (add-hook 'clojure-mode-hook 'rainbow-delimiters-mode))

(use-package parinfer
  :ensure t
  :init
  (setq parinfer-extensions
	'(defaults
	    evil
	    smart-tab
	    smart-yank))
  (add-hook 'clojurescript-mode-hook 'parinfer-mode)
  (add-hook 'clojurec-mode-hook 'parinfer-mode)
  (add-hook 'clojure-mode-hook 'parinfer-mode))


(use-package clj-refactor
  :ensure t
  :init
  (add-hook 'clojurescript-mode-hook 'clj-refactor-mode)
  (add-hook 'clojurec-mode-hook 'clj-refactor-mode)
  (add-hook 'clojure-mode-hook 'clj-refactor-mode))


(use-package cider
  :ensure t
  :init
  (setq cider-lein-command
	  (if (eq 'darwin system-type)
	      "/usr/local/bin/lein"
	    "/usr/bin/lein")
        cider-repl-display-help-banner nil))


(defun clojure/insert-region-in-repl ()
  "Insert region in repl."
  (interactive)
  (cider-insert-region-in-repl (region-beginning) (region-end))
  (cider-repl-return)
  (evil-window-prev 1))

(defun clojure/insert-defun-in-repl ()
  "Insert 'top-level' form in repl."
  (interactive)
  (cider-insert-defun-in-repl)
  (cider-repl-return)
  (evil-window-prev 1))

(defun clojure/insert-last-sexp-in-repl ()
  "Insert last sexp in repl."
  (interactive)
  (cider-insert-last-sexp-in-repl)
  (cider-repl-return)
  (evil-window-prev 1))

(defun clojure/find-var-at-point ()
  "Find clojure var for symbol at point."
  (interactive)
  (cider-find-var (symbol-at-point)))

(defun clojure/doc-at-point ()
  "Show cider-doc for symbol at point."
  (interactive)
  (cider-doc (symbol-at-point)))

(provide 'init-clojure)
