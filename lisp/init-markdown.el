
(use-package flyspell
  :ensure t
  :init (progn
	    (add-hook 'markdown-mode-hook 'flyspell-mode)))

(use-package markdown-mode
  :ensure t
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (progn
	  (setq markdown-command "multimarkdown")
	  (add-hook 'markdown-mode 'flyspell-mode)))


(provide 'init-markdown)
