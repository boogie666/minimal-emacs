(use-package restart-emacs :ensure t)
(use-package projectile
  :ensure t
  :diminish projectile-mode
  :init
  (progn
    (setq projectile-completion-system 'ivy)
    (projectile-mode +1)))



(use-package dashboard
  :ensure t
  :config (dashboard-setup-startup-hook)
  :init
  (progn
    (setq dashboard-startup-banner "~/.emacs.d/logo.png")
    (setq dashboard-set-file-icons t)
    (setq dashboard-set-heading-icons t)
    (setq dashboard-items '((projects . 5)
			    (recents  . 5)
			    (bookmarks . 5)))
    (setq dashboard-center-content t)))



(use-package evil-leader
  :ensure t
  :init

  (global-set-key (kbd "<escape>") 'keyboard-escape-quit)

  (global-evil-leader-mode 1)
  (evil-leader/set-leader "<SPC>"))

(use-package evil
  :ensure t
  :after evil-leader
  :init (evil-mode t))


(defun neotree--change-root (full-path &optional arg)
  (neo-global--open-dir full-path))



(defun neotree--open-file-and-hide (full-path &optional arg)
  (neo-open-file full-path arg)
  (neotree-hide))

(defun neotree--enter-and-hide (&optional arg)
  (interactive "P")
  (neo-buffer--execute arg 'neotree--open-file-and-hide 'neotree--change-root))

(defun neotree--enter-and-hide-tab (&optional arg)
  (interactive "P")
  (neo-buffer--execute arg 'neotree--open-file-and-hide 'neo-open-dir))

(use-package neotree
  :ensure t
  :after evil
  :init
  (progn
    (setq neo-theme (if (display-graphic-p) 'icons 'arrow))
    (add-hook 'neotree-mode-hook
          (lambda ()
            (define-key evil-normal-state-local-map (kbd "TAB") 'neotree--enter-and-hide-tab)
            (define-key evil-normal-state-local-map (kbd "SPC") 'neotree--enter-and-hide)
            (define-key evil-normal-state-local-map (kbd "q") 'neotree-hide)
            (define-key evil-normal-state-local-map (kbd "RET") 'neotree--enter-and-hide)))))



(use-package which-key
  :ensure t
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.4)
  (setq which-key-enable-extended-define-key t)
  :hook '(after-init . which-key-mode))

(use-package avy
  :ensure t
  :init
  (progn
    (setq avy-all-windows-alt t)
    (setq avy-highlight-first t)
    (setq avy-background t)))



(use-package linum-relative
  :ensure t
  :diminish linum-relative-mode
  :init
  (progn
    (setq linum-relative-format " %3s ")
    (add-hook 'prog-mode-hook 'linum-relative-mode)
    (add-hook 'markdown-mode-hook 'linum-relative-mode)))

(use-package company-flx
  :ensure t
  :diminish company-flx-mode
  :init
  (with-eval-after-load 'company
    (company-flx-mode +1)))

(use-package company
  :ensure t
  :diminish company-mode
  :init
  (setq company-idle-delay 0.3)
  (global-company-mode t))

(use-package thingatpt :ensure t)

(use-package counsel
  :ensure t
  :diminish counsel-mode
  :init
  (counsel-mode t)
  (with-eval-after-load 'counsel
    (setq ivy-use-virtual-buffers t)
    (let ((done (where-is-internal #'ivy-done     ivy-minibuffer-map t))
	    (alt  (where-is-internal #'ivy-alt-done ivy-minibuffer-map t)))
	(define-key counsel-find-file-map done #'ivy-alt-done)
	(define-key counsel-find-file-map alt  #'ivy-done))))


(use-package exec-path-from-shell
  :ensure t
  :init
  (progn
    (exec-path-from-shell-initialize)))

(defun --bind-key (f k d)
  "Bind key K with def D using function F."
  (funcall f k (cdr d))
  (which-key-add-key-based-replacements (concat evil-leader/leader " " k) (car d)))

(defun bind-global-keys (key def &rest bindings)
  "Binds (KEY DEF) pairs (as BINDINGS) to global."
  (while key
    (--bind-key 'evil-leader/set-key key def)
    (setq key (pop bindings)
	  def (pop bindings))))

(defun bind-mode-keys (modes key def &rest bindings)
  "Bindes KEY and DEF to give MODES."
  (while key
    (dolist (m modes)
      (--bind-key (lambda (k d)
		    (evil-leader/set-key-for-mode m k d))
		  key def))
    (setq key (pop bindings)
	  def (pop bindings))))




(defun emacs/kill-current-buffer ()
  "Kill current buffer."
  (interactive)
  (kill-buffer (current-buffer)))

(defun emacs/switch-to-buffer-alternate ()
  "Switch to previous buffer."
  (interactive)
  (switch-to-buffer (other-buffer)))

(defun ansi-term/new-window-bottom ()
  "Open new window in bottom split."
  (interactive)
  (split-window-vertically)
  (evil-window-down 1)
  (ansi-term "/bin/bash"))

(defun flycheck/conditional-toggle ()
  "Toggle flycheck syntax checker."
  (unless (string-suffix-p "project.clj" (buffer-file-name))
    (flycheck-mode)))

(defun ivy--run-search (title search action)
  (let* ((ivy--search-cmd search)
	 (max-dir-length 75)
	 (min-chars 2)
	 (directory (counsel-read-directory-name title (projectile-project-root))))
    (ivy-read (format "Find in [%s]: "
		      (if (< (length directory) max-dir-length)
		         directory
	         	 (concat
			    "..." (substring directory
					    (- (length directory) max-dir-length)
					    (length directory)))))
	    (lambda (string &optional _pred &rest unsued)
		(when (>= (length string) min-chars)
		    (counsel--async-command (format ivy--search-cmd
						   (counsel--elisp-to-pcre (setq ivy--old-re (ivy--regex string)))
						   directory)))
		nil)
	    :dynamic-collection t
	    :history 'counsel-git-grep-history
	    :action action
	    :caller 'ivy--run-search
	    :unwind (lambda ()
			(counsel-delete-process)
			(swiper--cleanup)))))

(defun ivy/search ()
  "Search in given directory using ag."
  (interactive)
  (ivy--run-search "Start search in directory: " "ag -i --nocolor --nogroup %S %s" #'counsel-git-grep-action))

(defun ivy/fuzzy-open ()
  "Fuzzy file open with fzf"
  (interactive)
  (ivy--run-search "Find files in directory: " "ag --nocolor --nogroup -g %S %s" #'find-file))

(defun toggle-maximize-buffer () "Toggle maximize buffer"
  (interactive)
  (if (= 1 (length (window-list)))
    (set-window-configuration my-saved-window-configuration)
    (progn
      (setq my-saved-window-configuration (current-window-configuration))
      (delete-other-windows))))

(defun ivy/bookmarks ()
  (interactive)
  (ignore-errors
    (bookmark-jump (ivy-read "Open bookmark: "
			(bookmark-all-names)))))

(defun ivy/bookmarks-delete ()
  (interactive)
  (ignore-errors
    (bookmark-delete (ivy-read "Delete bookmark: "
			(bookmark-all-names)))))

(defun font/adjust ()
  "Adjust the font dynamicaly."
  (interactive)
  (text-scale-decrease 1)
  (text-scale-adjust 1))


(use-package editorconfig
  :ensure t
  :config
  (editorconfig-mode 1))


(provide 'init-global)
