(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("e29a6c66d4c383dbda21f48effe83a1c2a1058a17ac506d60889aba36685ed94" "669e02142a56f63861288cc585bee81643ded48a19e36bfdf02b66d745bcc626" "855eb24c0ea67e3b64d5d07730b96908bac6f4cd1e5a5986493cbac45e9d9636" default))
 '(fira-code-mode-disabled-ligatures '("[]" "#{" "#(" "#_" "#_(" "x") t)
 '(package-selected-packages
   '(neotree fira-code-mode markdown-mode simple-modeline diminish which-key use-package smart-mode-line-atom-one-dark-theme restart-emacs rainbow-delimiters projectile parinfer linum-relative flycheck-clj-kondo evil-leader dashboard counsel company-flx clj-refactor avy atom-one-dark-theme all-the-icons ag))
 '(safe-local-variable-values
   '((cider-repl-display-help-banner)
     (cider-refresh-after-fn . "reloaded.repl/resume")
     (cider-refresh-before-fn . "reloaded.repl/suspend")
     (cider-default-cljs-repl . "(do (user/go) (user/cljs-repl))")
     (cider-cljs-lein-repl . "(do (user/go) (user/cljs-repl))")
     (cider-ns-refresh-after-fn . "reloaded.repl/resume")
     (cider-ns-refresh-before-fn . "reloaded.repl/suspend"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


(provide 'init-local)
